package com.gregmilitello.project0;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnSpotify = (Button) findViewById(R.id.btnSpotify);
        Button btnScores = (Button) findViewById(R.id.btnScores);
        Button btnLibrary = (Button) findViewById(R.id.btnLibrary);
        Button btnBuild = (Button) findViewById(R.id.btnBuild);
        Button btnXyz = (Button) findViewById(R.id.btnXyz);
        Button btnCapstone = (Button) findViewById(R.id.btnCapstone);
        btnSpotify.setOnClickListener(this);


    }

    @Override
    public void onClick(View v){
        switch (v.getId())
        {
            case R.id.btnSpotify:
                Toast.makeText(this,"This button will launch my Spotify Streamer app!", Toast.LENGTH_SHORT ).show();
                break;
            case R.id.btnScores:
                Toast.makeText(this,"This button will launch my Scores app!", Toast.LENGTH_SHORT ).show();
                break;
            case R.id.btnLibrary:
                Toast.makeText(this,"This button will launch my Library app!", Toast.LENGTH_SHORT ).show();
                break;
            case R.id.btnBuild:
                Toast.makeText(this,"This button will launch my Build it Bigger app!", Toast.LENGTH_SHORT ).show();
                break;
            case R.id.btnXyz:
                Toast.makeText(this,"This button will launch my XYZ Reader app!", Toast.LENGTH_SHORT ).show();
                break;
            case R.id.btnCapstone:
                Toast.makeText(this,"This button will launch my Capstone app!", Toast.LENGTH_SHORT ).show();
                break;
        }

    }
}
